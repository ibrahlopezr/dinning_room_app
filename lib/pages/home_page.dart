import 'dart:ui';

import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,

      // appBar: AppBar(
      //   title: Text('Material App Bar'),
      // ),
      //backgroundColor: Color(0xFF21BFBD),
      body: Stack(
        children: [
          //SizedBox(height: 25.0),
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/login-bg.jpg"),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.30), BlendMode.darken),
              ),
            ),
            //child: Image.asset('assets/images/login-bg.jpg', ),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * .2),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "¡BIENVENIDO!",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 30.0,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          // Card principal
          CardLogin(),
        ],
      ),
    );
  }
}

class CardLogin extends StatelessWidget {
  const CardLogin({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * .30,
        ),
        height: MediaQuery.of(context).size.height - 130.0,
        decoration: BoxDecoration(
          color: Color(0xfffafafa),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(60.0),
            topRight: Radius.circular(60.0),
          ),
        ),
        child: ListView(
          primary: false,
          padding: EdgeInsets.only(left: 25.0, right: 20.0),
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: Container(
                height: MediaQuery.of(context).size.height - 300.0,
                child: Container(
                  child: ListView(
                    children: [
                      Center(
                        child: Text(
                          "LOGIN",
                          style: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 28,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      //Underline login title
                      UnderlineLoginTitle(),
                      // Form login
                      FormLogin(),
                    ],
                  ),
                ),
              ),
            ),
            Logo(),
          ],
        ),
      ),
    );
  }
}

class UnderlineLoginTitle extends StatelessWidget {
  const UnderlineLoginTitle({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 12.0,
        bottom: 25.0,
      ),
      child: Container(
        width: double.infinity,
        height: 2,
        color: Colors.redAccent,
        margin: const EdgeInsets.only(right: 4),
      ),
    );
  }
}

class Logo extends StatelessWidget {
  const Logo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * .2,
      height: MediaQuery.of(context).size.height * .15,
      // decoration: BoxDecoration(
      //   image: DecorationImage(
      //     image: AssetImage("assets/images/logo.png"),
      //     fit: BoxFit.cover,
      //     colorFilter: ColorFilter.mode(
      //       Colors.white.withOpacity(0.30),
      //       BlendMode.darken,
      //     ),
      //   ),
      // ),
      child: Image.asset(
        'assets/images/logo.png',
        width: 100.0,
        //height: 50,
        fit: BoxFit.fitHeight,
      ),
    );
  }
}

class FormLogin extends StatelessWidget {
  const FormLogin({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 12.0,
              left: 20.0,
              right: 20.0,
              bottom: 12.0,
            ),
            child: Material(
              borderRadius: BorderRadius.circular(15.0),
              elevation: 8.0,
              //shadowColor: Colors.white,
              child: TextFormField(
                decoration: InputDecoration(
                  suffixIcon: Icon(
                    Icons.person_outline_rounded,
                    color: Colors.redAccent,
                    size: 30,
                  ),
                  hintText: 'Usuario',
                  fillColor: Color(0xfffffdfd),
                  hintStyle: TextStyle(
                      color: Colors.redAccent, fontWeight: FontWeight.bold),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.white,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 12.0,
              left: 20.0,
              right: 20.0,
              bottom: 12.0,
            ),
            child: Material(
              borderRadius: BorderRadius.circular(15.0),
              elevation: 8.0,
              //shadowColor: Colors.white,
              child: TextFormField(
                decoration: InputDecoration(
                  suffixIcon: Icon(
                    Icons.remove_red_eye_outlined,
                    color: Colors.redAccent,
                    size: 30,
                  ),
                  hintText: 'Contraseña',
                  fillColor: Color(0xfffffdfd),
                  hintStyle: TextStyle(
                    color: Colors.redAccent,
                    fontWeight: FontWeight.bold,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.white,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 12.0,
              left: 20.0,
              right: 20.0,
              bottom: 12.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text("¿Olvidaste tu contraseña?"),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 12.0,
              left: 20.0,
              right: 20.0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: ButtonTheme(
                    minWidth: 150.0,
                    height: 50.0,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      color: Color(0xffff3333),
                      onPressed: () {},
                      child: Text(
                        "LOGIN",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
